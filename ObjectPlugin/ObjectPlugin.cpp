#include "stdafx.h"
#include "ObjectPlugin.h"
#include "text.h"

char szInformation[] =	"MiniBlink (WKE) Object for AutoPlay Media Studio\r\nCreated by Pabloko & Ini- amsspecialist.com\r\n";
char szObjectName[] =	"MiniBlink";
char szVer[] =			"1.0.0.0";

OBJECTPLUGIN_API CIRPluginObject* irPlg_Object_CreateObject()
{
	return new CIRPluginObject();
}

OBJECTPLUGIN_API void irPlg_Object_DeleteObject(CIRPluginObject* pObject)
{
	if (pObject)
	{
		delete pObject;
		pObject = NULL;
	}
}


OBJECTPLUGIN_API int irPlg_GetPluginName(char* szBuffer, int* pnBufferSize)
{
	int nLength = lstrlen(szObjectName);
	if (*pnBufferSize < nLength) {
		*pnBufferSize = nLength;
		return -1;
	} else {
		memset(szBuffer, 0, *pnBufferSize);
		lstrcpy(szBuffer, szObjectName);
		return nLength;
	}
}

OBJECTPLUGIN_API int irPlg_GetPluginVersion(char* szBuffer, int* pnBufferSize)
{
	int nLength = lstrlen(szVer);
	if (*pnBufferSize < nLength) {
		*pnBufferSize = nLength;
		return -1;
	} else {
		memset(szBuffer, 0, *pnBufferSize);
		lstrcpy(szBuffer, szVer);
		return nLength;
	}
}

OBJECTPLUGIN_API int irPlg_GetIRPluginObjectVersion()
{
	return 1;// Always returns this, do not remove or modify...
}

OBJECTPLUGIN_API int irPlg_GetAuthorInfo(char* szBuffer, int* pnBufferSize)
{
	int nLength = lstrlen(szInformation);
	if (*pnBufferSize < nLength) {
		*pnBufferSize = nLength;
		return -1;
	} else {
		memset(szBuffer, 0, *pnBufferSize);
		lstrcpy(szBuffer, szInformation);
		return nLength;
	}
}

OBJECTPLUGIN_API int irPlg_GetPluginActionXML(char* szBuffer, int* pnBufferSize)
{
	//memset(szXML, 0, 10000);
	int nLength = lstrlen((char*)actions_xml);
	if (*pnBufferSize < nLength) {
		*pnBufferSize = nLength;
		return -1;
	} else {
		memset(szBuffer, 0, *pnBufferSize);
		lstrcpy(szBuffer, (char*)actions_xml);
		return nLength;
	}
}

OBJECTPLUGIN_API bool irPlg_ShowHelpForAction(char* lpszActionName, char* lpszPluginPath, HWND hParentWnd)
{
	CString strHelpFile;
	CString strFullHelpLink;
	strHelpFile.Format("%s\\Help.html", lpszPluginPath);
	strFullHelpLink.Format("%s#%s", strHelpFile, lpszActionName);
	if (GetFileAttributes(strHelpFile) == -1)
		return FALSE;
	else
		ShellExecute(hParentWnd, "open", strHelpFile, NULL, NULL, SW_NORMAL);
	return TRUE;
}

OBJECTPLUGIN_API bool irPlg_ShowHelpForPlugin(char* lpszPluginPath, HWND hParentWnd)
{
	CString strHelpFile;
	strHelpFile.Format("%s\\Help.html", lpszPluginPath);
	if (GetFileAttributes(strHelpFile) == -1)
		return FALSE;
	else
		ShellExecute(hParentWnd, "open", strHelpFile, NULL, NULL, SW_NORMAL);
	return TRUE;
}

OBJECTPLUGIN_API bool irPlg_ValidateLicense(char* lpszLicenseInfo)
{
	return TRUE;
}

OBJECTPLUGIN_API int irPlg_GetLuaVersion(char* szBuffer, int* pnBufferSize)
{
	int nLength = lstrlen(LUA_VERSION);
	if (*pnBufferSize < nLength) {
		*pnBufferSize = nLength;
		return -1;
	} else {
		memset(szBuffer, 0, *pnBufferSize);
		lstrcpy(szBuffer, LUA_VERSION);
		return nLength;
	}
}

OBJECTPLUGIN_API int irPlg_GetSDKVersion(void)
{
	return 2; //dont touch this
}

OBJECTPLUGIN_API BOOL irPlg_Object_TranslateMessage(MSG* pMsg)
{
	//return theApp.PreTranslateMessage(pMsg);
	return FALSE;
}
OBJECTPLUGIN_API int irPlg_GetDependencies(char* szBuffer, int* pnBufferSize) {	char * ndll = "node.dll|npswf32-11.5.dll|pdfium.dll";	int nLength = lstrlen(ndll);
	if (*pnBufferSize < nLength) {
		*pnBufferSize = nLength;
		return FALSE;
	} else {
		memset(szBuffer, 0, *pnBufferSize);
		lstrcpy(szBuffer, ndll);
		return TRUE;
	}}
void Log(CHAR * szFormat, ...)
{
	char tmp_buf[512];
	memset(tmp_buf, 0, 512);
	va_list args;
	va_start(args, szFormat);
	vsprintf_s(tmp_buf, szFormat, args);
	va_end(args);
	OutputDebugString(tmp_buf);
}
