#include "stdafx.h"
#include "IRPluginObject.h"

struct luacb_t {
	int pFn;
	CIRPluginObject* cip;
};

CIRPluginObject::CIRPluginObject()
{
	memset(m_szObjectID, 0, 100);
	lstrcpy(m_szObjectID, "OBJECT_PLUGIN_WKE");
	//lstrcpy(m_szPropertiesString, "");
	for (int i = 0; i < NUM_OB_EVENTS; i++)
		m_pEvents[i] = NULL;
	m_pEvents[0] = new IRPluginEventInfo;
	lstrcpy(m_pEvents[0]->m_szName, "OnPreload");
	lstrcpy(m_pEvents[0]->m_szArgs, "");
	m_pEvents[1] = new IRPluginEventInfo;
	lstrcpy(m_pEvents[1]->m_szName, "OnConsole");
	lstrcpy(m_pEvents[1]->m_szArgs, "string e_Message, number e_Level, string e_SourceName, number e_SourceLine, string e_StackTrace"); 
	m_pEvents[2] = new IRPluginEventInfo;
	lstrcpy(m_pEvents[2]->m_szName, "OnAlert");
	lstrcpy(m_pEvents[2]->m_szArgs, "string e_Msg");
	m_pEvents[3] = new IRPluginEventInfo;
	lstrcpy(m_pEvents[3]->m_szName, "OnTitleChanged");
	lstrcpy(m_pEvents[3]->m_szArgs, "string e_Title");
	m_pEvents[4] = new IRPluginEventInfo;
	lstrcpy(m_pEvents[4]->m_szName, "OnURLChanged");
	lstrcpy(m_pEvents[4]->m_szArgs, "string e_Url");
	m_pEvents[5] = new IRPluginEventInfo;
	lstrcpy(m_pEvents[5]->m_szName, "OnConfirmBox");
	lstrcpy(m_pEvents[5]->m_szArgs, "string e_Msg, *bool e_Confirm");
	m_pEvents[6] = new IRPluginEventInfo;
	lstrcpy(m_pEvents[6]->m_szName, "OnPromptBox");
	lstrcpy(m_pEvents[6]->m_szArgs, "string e_Msg, string e_DefaultResult, *string e_Result, *bool e_Cancel");
	m_pEvents[7] = new IRPluginEventInfo;
	lstrcpy(m_pEvents[7]->m_szName, "OnNavigation");
	lstrcpy(m_pEvents[7]->m_szArgs, "string e_Url, number e_Type, *bool e_Confirm");
	m_pEvents[8] = new IRPluginEventInfo;
	lstrcpy(m_pEvents[8]->m_szName, "OnDocumentReady");
	lstrcpy(m_pEvents[8]->m_szArgs, "");
	m_pEvents[9] = new IRPluginEventInfo;
	lstrcpy(m_pEvents[9]->m_szName, "OnDownload");
	lstrcpy(m_pEvents[9]->m_szArgs, "string e_Url, *bool e_Confirm");

	m_hWnd = NULL;
	window = NULL;
	m_pLuaState = NULL;
	luaen = false;
}

CIRPluginObject::~CIRPluginObject()
{
	HideInternalObject();
	if (wkeFinalize!=NULL)
		wkeFinalize();
}

HWND CIRPluginObject::GetWindowHandle()
{
	return m_hWnd;
}

void CIRPluginObject::DrawDesign(HDC hDC, HWND hMainWnd, RECT rcObRect, BOOL bVisible, BOOL bEnabled)
{
	if (bVisible) {
		Log("DrawDesign ParentHwnd=%x x=%d y=%d width=%d height=%d", hMainWnd, rcObRect.left, rcObRect.top, rcObRect.right - rcObRect.left, rcObRect.bottom - rcObRect.top);
		DWORD dwStyle;
		dwStyle = WS_CHILD | WS_VISIBLE | WS_BORDER;
		if (!bEnabled) 
			dwStyle |= WS_DISABLED;
		if (m_hWnd == NULL) 
			m_hWnd = CreateWindow("static", "", dwStyle, rcObRect.left, rcObRect.top, rcObRect.right - rcObRect.left, rcObRect.bottom - rcObRect.top, hMainWnd, (HMENU)(0), (HINSTANCE)GetWindowLong(m_hWnd, GWL_HINSTANCE), NULL);
		SetWindowText(m_hWnd, "MiniBlink (WKE) Object Plugin by @Pabloko, @Ini\nminiblink49 by Weolar\nhttps://amsspecialist.com\nhttps://miniblink.net");
		MoveWindow(m_hWnd, rcObRect.left, rcObRect.top, rcObRect.right - rcObRect.left, rcObRect.bottom - rcObRect.top, TRUE);
	} else
		HideInternalObject();
}

void jstolua(lua_State* ls, jsExecState es, jsValue r, int customtyp = -1) {
	if (customtyp == -1)
		customtyp = jsTypeOf(r);
	switch (customtyp) {
	case jsType::JSTYPE_STRING:
		lua_pushstring(ls, jsToString(es, r));
		break;
	case jsType::JSTYPE_BOOLEAN:
		lua_pushboolean(ls, jsToBoolean(es, r));
		break;
	case jsType::JSTYPE_NUMBER:
		lua_pushnumber(ls, jsToDouble(es, r));
		break;
	case jsType::JSTYPE_FUNCTION:
		lua_pushlightuserdata(ls, (void*)r);
		break;
	case jsType::JSTYPE_ARRAY:
	{
		int len = jsGetLength(es, r);
		lua_createtable(ls, len, 0);
		for (int i = 1; i <= len; i++) {
			jstolua(ls, es, jsGetAt(es, r, i - 1));
			lua_rawseti(ls, -2, i);
		}
	}
		break;
	case jsType::JSTYPE_OBJECT:
	{
		jsKeys* jk = jsGetKeys(es, r);
		lua_createtable(ls, 0, jk->length);
		for (int i = 1; i <= jk->length; i++) {
			lua_pushstring(ls, jk->keys[i - 1]);
			jstolua(ls, es, jsGet(es, r, jk->keys[i - 1]));
			lua_rawset(ls, -3);
		}
	}
		break;
	case jsType::JSTYPE_NULL:
	case jsType::JSTYPE_UNDEFINED:
		lua_pushnil(ls);
		break;
	default:
		lua_pushlightuserdata(ls, (void*)r);
		break;
	}
}

jsValue luatojs(lua_State* ls, jsExecState es, int stack_pos=-1, int customtyp=-1) {
	jsValue ret = jsNull();
	if (customtyp == -1)
		customtyp = lua_type(ls, stack_pos);
	switch (customtyp) {
	case LUA_TNUMBER:
		ret = jsDouble(lua_tonumber(ls, stack_pos));
		break;
	case LUA_TSTRING:
		ret = jsString(es, lua_tostring(ls, stack_pos));
		break;
	case LUA_TBOOLEAN:
		ret = jsBoolean(lua_toboolean(ls, stack_pos));
		break;
	case LUA_TUSERDATA:
	case LUA_TLIGHTUSERDATA:
		ret = (jsValue)lua_touserdata(ls, stack_pos);
		break;
	case LUA_TTABLE:
	{
		int n = luaL_getn(ls, -1);
		if (n > 0) {
			ret = jsEmptyArray(es);
			for (int i = 1; i <= n; i++) {
				lua_rawgeti(ls, -1, i);
				jsSetAt(es, ret, i - 1, luatojs(ls, es));
				lua_pop(ls, 1);
			}
		} else {
			ret = jsEmptyObject(es);
			lua_pushvalue(ls, stack_pos);
			lua_pushnil(ls);
			while (lua_next(ls, -2)) {
				lua_pushvalue(ls, -2);
				const char *key = lua_tostring(ls, -1);
				//const char *value = lua_tostring(ls, -2);
				jsSet(es, ret, key, luatojs(ls, es, -2));
				lua_pop(ls, 2);
			}
			lua_pop(ls, 1);
		}
	}
		break;
	default:
		ret = jsNull();
		break;
	}
	return ret;
}

void OnConsoleCb(wkeWebView webView, void* param, wkeConsoleLevel level, const wkeString message, const wkeString sourceName, unsigned sourceLine, const wkeString stackTrace) {
	CIRPluginObject* cip = (CIRPluginObject*)param;
	lua_pushstring(cip->m_pLuaState, wkeGetString(message));
	lua_setfield(cip->m_pLuaState, LUA_GLOBALSINDEX, "e_Message");
	lua_pushinteger(cip->m_pLuaState, level);
	lua_setfield(cip->m_pLuaState, LUA_GLOBALSINDEX, "e_Level");
	lua_pushstring(cip->m_pLuaState, wkeGetString(sourceName));
	lua_setfield(cip->m_pLuaState, LUA_GLOBALSINDEX, "e_SourceName");
	lua_pushinteger(cip->m_pLuaState, sourceLine);
	lua_setfield(cip->m_pLuaState, LUA_GLOBALSINDEX, "e_SourceLine");
	lua_pushstring(cip->m_pLuaState, wkeGetString(stackTrace));
	lua_setfield(cip->m_pLuaState, LUA_GLOBALSINDEX, "e_StackTrace");
	cip->FireEvent("OnConsole", "");
}

void onTitleChangedCb(wkeWebView webView, void* param, const wkeString title) {
	CIRPluginObject* cip = (CIRPluginObject*)param;
	lua_pushstring(cip->m_pLuaState, wkeGetString(title));
	lua_setfield(cip->m_pLuaState, LUA_GLOBALSINDEX, "e_Title");
	cip->FireEvent("OnTitleChanged", "");
}

void MsgBoxAlertCb(wkeWebView webView, void* param, const wkeString msg) {
	CIRPluginObject* cip = (CIRPluginObject*)param;
	lua_pushstring(cip->m_pLuaState, wkeGetString(msg));
	lua_setfield(cip->m_pLuaState, LUA_GLOBALSINDEX, "e_Msg");
	cip->FireEvent("OnAlert", "");
}

void onURLChangedCb(wkeWebView webView, void* param, const wkeString url) {
	CIRPluginObject* cip = (CIRPluginObject*)param;
	lua_pushstring(cip->m_pLuaState, wkeGetString(url));
	lua_setfield(cip->m_pLuaState, LUA_GLOBALSINDEX, "e_Url");
	cip->FireEvent("OnURLChanged", "");
	
}

bool onConfirmBoxCb(wkeWebView webView, void* param, const wkeString msg) {
	CIRPluginObject* cip = (CIRPluginObject*)param;
	lua_pushstring(cip->m_pLuaState, wkeGetString(msg));
	lua_setfield(cip->m_pLuaState, LUA_GLOBALSINDEX, "e_Msg");
	lua_pushboolean(cip->m_pLuaState, false);
	lua_setfield(cip->m_pLuaState, LUA_GLOBALSINDEX, "e_Confirm");
	cip->FireEvent("OnConfirmBox", "");
	lua_getfield(cip->m_pLuaState, LUA_GLOBALSINDEX, "e_Confirm");
	return lua_toboolean(cip->m_pLuaState, -1);
}

bool onPromptBoxCb(wkeWebView webView, void* param, const wkeString msg, const wkeString defaultResult, wkeString result) {
	CIRPluginObject* cip = (CIRPluginObject*)param;
	lua_pushstring(cip->m_pLuaState, wkeGetString(msg));
	lua_setfield(cip->m_pLuaState, LUA_GLOBALSINDEX, "e_Msg");
	lua_pushstring(cip->m_pLuaState, wkeGetString(defaultResult));
	lua_setfield(cip->m_pLuaState, LUA_GLOBALSINDEX, "e_DefaultResult");
	lua_pushstring(cip->m_pLuaState, wkeGetString(result));
	lua_setfield(cip->m_pLuaState, LUA_GLOBALSINDEX, "e_Result");
	lua_pushboolean(cip->m_pLuaState, false);
	lua_setfield(cip->m_pLuaState, LUA_GLOBALSINDEX, "e_Cancel");
	cip->FireEvent("OnPromptBox", "");
	lua_getfield(cip->m_pLuaState, LUA_GLOBALSINDEX, "e_Result");
	const char* ret = lua_tostring(cip->m_pLuaState, -1);
	wkeSetString(result, ret, strlen(ret));
	lua_getfield(cip->m_pLuaState, LUA_GLOBALSINDEX, "e_Cancel");
	return lua_toboolean(cip->m_pLuaState, -1);
}

bool onNavigationCb(wkeWebView webView, void* param, wkeNavigationType navigationType, wkeString url){
	CIRPluginObject* cip = (CIRPluginObject*)param;
	lua_pushinteger(cip->m_pLuaState, navigationType);
	lua_setfield(cip->m_pLuaState, LUA_GLOBALSINDEX, "e_Type");
	lua_pushstring(cip->m_pLuaState, wkeGetString(url));
	lua_setfield(cip->m_pLuaState, LUA_GLOBALSINDEX, "e_Url");
	lua_pushboolean(cip->m_pLuaState, true);
	lua_setfield(cip->m_pLuaState, LUA_GLOBALSINDEX, "e_Confirm");
	cip->FireEvent("OnNavigation", "");
	lua_getfield(cip->m_pLuaState, LUA_GLOBALSINDEX, "e_Confirm");
	return lua_toboolean(cip->m_pLuaState, -1);
}

void onDocReadyCb(wkeWebView webView, void* param){
	CIRPluginObject* cip = (CIRPluginObject*)param;
	cip->FireEvent("OnDocumentReady", "");
}

bool onDownloadCb(wkeWebView webView, void* param, const char* url) {
	CIRPluginObject* cip = (CIRPluginObject*)param;
	lua_pushstring(cip->m_pLuaState, url);
	lua_setfield(cip->m_pLuaState, LUA_GLOBALSINDEX, "e_Url");
	lua_pushboolean(cip->m_pLuaState, false);
	lua_setfield(cip->m_pLuaState, LUA_GLOBALSINDEX, "e_Confirm");

	cip->FireEvent("OnDownload", "");
	lua_getfield(cip->m_pLuaState, LUA_GLOBALSINDEX, "e_Confirm");
	return lua_toboolean(cip->m_pLuaState, -1);
}

jsValue load_lua(jsExecState es, void* param) {
	CIRPluginObject* cip = (CIRPluginObject*)param;
	if (!cip->luaen) 
		return jsThrowException(es, "LUA Callback is disabled, use LuaEnable");
	jsValue ret = jsNull();
	if (jsArgCount(es) > 0 && jsArgType(es,0)==jsType::JSTYPE_STRING) {
		const char* script = jsToString(es, jsArg(es, 0));
		luaL_dostring(cip->m_pLuaState, script);
		if (lua_gettop(cip->m_pLuaState) > 0)
			ret = luatojs(cip->m_pLuaState, es);
	}
	return ret;
}

jsValue load_luacbgsetter(jsExecState es, void* param) {
	luacb_t* cb = (luacb_t*)param;
	jsValue ret = jsNull();
	lua_rawgeti(cb->cip->m_pLuaState, LUA_REGISTRYINDEX, cb->pFn);
	if (lua_isfunction(cb->cip->m_pLuaState, -1))
	{
		lua_pushlightuserdata(cb->cip->m_pLuaState, (void*)es);
		if (lua_pcall(cb->cip->m_pLuaState, 1, 1, 0) != 0)
			Log("Lua error: %s", lua_tostring(cb->cip->m_pLuaState, -1));
		else
			ret = luatojs(cb->cip->m_pLuaState, es);
	}
	else
		lua_remove(cb->cip->m_pLuaState, -1);
	return ret;
}

void CIRPluginObject::DrawRuntime(HDC hDC, HWND hMainWnd, RECT rcObRect, BOOL bVisible, BOOL bEnabled)
{
	if (m_hWnd == NULL) {
		wchar_t DllPath[MAX_PATH] = { 0 };
		GetModuleFileNameW((HINSTANCE)&__ImageBase, DllPath, _countof(DllPath));
		wchar_t DllPath_dir[MAX_PATH] = { 0 };
		wchar_t DllPath_drive[5] = { 0 };
		_wsplitpath_s(DllPath, DllPath_drive, sizeof(DllPath_drive), DllPath_dir, sizeof(DllPath_dir), NULL, 0, NULL, 0);
		wsprintfW(DllPath, L"%s%snode.dll", DllPath_drive, DllPath_dir);
		wkeSetWkeDllPath(DllPath);
		wkeInitialize();
		wkeJsBindFunction("lua", load_lua, this, 1);
		window = wkeCreateWebWindow(WKE_WINDOW_TYPE_CONTROL, hMainWnd, rcObRect.left, rcObRect.top, rcObRect.right - rcObRect.left, rcObRect.bottom - rcObRect.top);
		m_hWnd = wkeGetWindowHandle(window);
		wsprintfW(DllPath, L"%s%s", DllPath_drive, DllPath_dir);
		wkeSetCookieJarPath(window, DllPath);
		wkeAddPluginDirectory(window, DllPath);
		wsprintfW(DllPath, L"%s%sLocalStorage", DllPath_drive, DllPath_dir);
		CreateDirectoryW(DllPath, NULL);
		wkeSetLocalStorageFullPath(window, DllPath);
		wkeSetLanguage(window, "EN-us");
		wkeOnAlertBox(window, MsgBoxAlertCb, this);
		wkeOnConsole(window, OnConsoleCb, this);
		wkeOnTitleChanged(window, onTitleChangedCb, this);
		wkeOnURLChanged(window, onURLChangedCb, this);
		wkeOnConfirmBox(window, onConfirmBoxCb, this);
		wkeOnPromptBox(window, onPromptBoxCb, this);
		wkeOnNavigation(window, onNavigationCb, this);
		wkeOnDocumentReady(window, onDocReadyCb, this);
		wkeOnDownload(window, onDownloadCb, this);
		wkeSetContextMenuEnabled(window, FALSE);
		wkeShowWindow(window, TRUE);
		FireEvent("OnPreload", "");
	}
	wkeShowWindow(window, bVisible);
	wkeEnableWindow(window, bEnabled);
	wkeMoveWindow(window, rcObRect.left, rcObRect.top, rcObRect.right - rcObRect.left, rcObRect.bottom - rcObRect.top);
}

void CIRPluginObject::HideInternalObject()
{
	if (window!=NULL)
		wkeDestroyWebWindow(window);
	window = NULL;
	if (m_hWnd!=NULL)
		DestroyWindow(m_hWnd);
	m_hWnd = NULL;
}

int CIRPluginObject::GetCustomProperties(char* szBuffer, int* pnBufferSize)
{
	int nLength = lstrlen(m_szPropertiesString);
	if (*pnBufferSize < nLength) {
		*pnBufferSize = nLength;
		return -1;
	} else {
		memset(szBuffer, 0, *pnBufferSize);
		lstrcpy(szBuffer, m_szPropertiesString);
		return nLength;
	}
}

void CIRPluginObject::SetCustomProperties(char* szPropsList)
{
	/*wsprintf(m_szPropertiesString, "%s", szPropsList);
	int nTokenNum = 0;
	char *token;
	token = strtok(szPropsList, "|");
	while (token != NULL) {
		switch (nTokenNum) {
		case 0: 
			sprintf_s(szUrl,"%s",token);
		break;
		}
		nTokenNum++;
		token = strtok(NULL, "|");
	}*/
}

BOOL CIRPluginObject::ShowProperties(char* szPluginFolder)
{
	Beep(500, 100);
	MessageBox(NULL,"No GUI mode\nUse WKE>OnPreload to add initial settings to the object.","WKE",MB_OK);
	Beep(1000, 100);
	//not using gui.no point.
	/*char dll_path[MAX_PATH];
	sprintf(dll_path, "%s\\MiniBlinkAMSUI.dll", szPluginFolder);
	HMODULE hMod = GetModuleHandle(dll_path);
	if (hMod == NULL)
		hMod = LoadLibrary(dll_path);
	delete dll_path;
	if (hMod == NULL) return FALSE;
	FARPROC fp = GetProcAddress(hMod, "ShowMiniBlinkGUI");
	char* prop = (*(char*(__stdcall **)(char*))&fp)(m_szPropertiesString);
	sprintf(m_szPropertiesString, "%s", prop);
	return TRUE;*/
	return FALSE;
}


BOOL CIRPluginObject::GetEvent(int nIndex, IRPluginEventInfo* pEventInfo)
{
	if ((nIndex >= 0) && nIndex < NUM_OB_EVENTS) {
		if (m_pEvents[nIndex]) {
			lstrcpy(pEventInfo->m_szName, m_pEvents[nIndex]->m_szName);
			lstrcpy(pEventInfo->m_szArgs, m_pEvents[nIndex]->m_szArgs);
			return TRUE;
		} else {
			return FALSE;
		}
	} else {
		return NULL;
	}
}

int CIRPluginObject::GetNumEvents()
{
	return NUM_OB_EVENTS;
}

#define GETCIP(id) CIRPluginObject* cip = IRLUA_PLUGIN_GetObjectPtr(ls, lua_tostring(ls, 1)); if (cip == NULL || lua_gettop(ls)!=id) return 0

int lua_wke_runjs(lua_State* ls) {
	GETCIP(2);
	jsExecState jex = wkeGlobalExec(cip->window);
	jsValue r = wkeRunJS(cip->window, lua_tostring(ls, 2));
	jstolua(cip->m_pLuaState, jex, r);
	return 1;
}

int lua_wke_gettitle(lua_State* ls) {
	GETCIP(1);
	lua_pushstring(ls, wkeGetTitle(cip->window));
	return 1;
}

int lua_wke_cangoback(lua_State* ls) {
	GETCIP(1);
	lua_pushboolean(ls, wkeCanGoBack(cip->window));
	return 1;
}

int lua_wke_goback(lua_State* ls) {
	GETCIP(1);
	wkeGoBack(cip->window);
	return 0;
}

int lua_wke_resize(lua_State* ls) {
	GETCIP(3);
	wkeResize(cip->window, lua_tointeger(ls, 2), lua_tointeger(ls, 3));
	return 0;
}

int lua_wke_getwidth(lua_State* ls) {
	GETCIP(1);
	lua_pushinteger(ls, wkeGetWidth(cip->window));
	return 1;
}

int lua_wke_getheight(lua_State* ls) {
	GETCIP(1);
	lua_pushinteger(ls, wkeGetHeight(cip->window));
	return 1;
}

int lua_wke_getcontentwidth(lua_State* ls) {
	GETCIP(1);
	lua_pushinteger(ls, wkeGetContentWidth(cip->window));
	return 1;
}

int lua_wke_getcontentheight(lua_State* ls) {
	GETCIP(1);
	lua_pushinteger(ls, wkeGetContentHeight(cip->window));
	return 1;
}

int lua_wke_cangoforward(lua_State* ls) {
	GETCIP(1);
	lua_pushboolean(ls, wkeCanGoForward(cip->window));
	return 1;
}

int lua_wke_goforward(lua_State* ls) {
	GETCIP(1);
	wkeGoForward(cip->window);
	return 0;
}

int lua_wke_selectall(lua_State* ls) {
	GETCIP(1);
	wkeEditorSelectAll(cip->window);
	return 0;
}

int lua_wke_unselect(lua_State* ls) {
	GETCIP(1);
	wkeEditorUnSelect(cip->window);
	return 0;
}

int lua_wke_copy(lua_State* ls) {
	GETCIP(1);
	wkeEditorCopy(cip->window);
	return 0;
}

int lua_wke_cut(lua_State* ls) {
	GETCIP(1);
	wkeEditorCut(cip->window);
	return 0;
}

int lua_wke_paste(lua_State* ls) {
	GETCIP(1);
	wkeEditorPaste(cip->window);
	return 0;
}

int lua_wke_delete(lua_State* ls) {
	GETCIP(1);
	wkeEditorDelete(cip->window);
	return 0;
}

int lua_wke_undo(lua_State* ls) {
	GETCIP(1);
	wkeEditorUndo(cip->window);
	return 0;
}

int lua_wke_redo(lua_State* ls) {
	GETCIP(1);
	wkeEditorRedo(cip->window);
	return 0;
}

int lua_wke_getcookie(lua_State* ls) {
	GETCIP(1);
	lua_pushstring(ls, wkeGetCookie(cip->window));
	return 1;
}

int lua_wke_setcookie(lua_State* ls) {
	GETCIP(3);
	wkeSetCookie(cip->window, lua_tostring(ls, 2), lua_tostring(ls, 3));
	return 0;
}

int lua_wke_setmediavolume(lua_State* ls) {
	GETCIP(2);
	wkeSetMediaVolume(cip->window, lua_tonumber(ls, 2));
	return 0;
}

int lua_wke_getmediavolume(lua_State* ls) {
	GETCIP(1);
	lua_pushnumber(ls, wkeGetMediaVolume(cip->window));
	return 1;
}

int lua_wke_firemouseevent(lua_State* ls) {
	GETCIP(5);
	UINT message = lua_tointeger(ls, 2);
	int x = lua_tointeger(ls, 3);
	int y = lua_tointeger(ls, 4);
	unsigned int flags = lua_tointeger(ls, 5);
	wkeFireMouseEvent(cip->window, message, x, y, flags);
	return 0;
}

int lua_wke_firemousewheelevent(lua_State* ls) {
	GETCIP(5);
	int x = lua_tointeger(ls, 2);
	int y = lua_tointeger(ls, 3);
	float delta = lua_tonumber(ls, 4);
	unsigned int flags = lua_tointeger(ls, 5);
	wkeFireMouseWheelEvent(cip->window, x, y, delta, flags);
	return 0;
}

int lua_wke_firekeyupevent(lua_State* ls) {
	GETCIP(4);
	int vKey_code = lua_tointeger(ls, 2);
	unsigned int flags = lua_tointeger(ls, 3);
	bool sysKey = lua_toboolean(ls, 4);
	wkeFireKeyUpEvent(cip->window, vKey_code, flags, sysKey);
	return 0;
}

int lua_wke_firedownevent(lua_State* ls) {
	GETCIP(4);
	int vKey_code = lua_tointeger(ls, 2);
	unsigned int flags = lua_tointeger(ls, 3);
	bool sysKey = lua_toboolean(ls, 4);
	wkeFireKeyDownEvent(cip->window, vKey_code, flags, sysKey);
	return 0;
}

int lua_wke_firewindowsmessage(lua_State* ls) {
	GETCIP(5);
	int hWnd = lua_tointeger(ls, 2);
	UINT message = lua_tointeger(ls, 3);
	int wParam = lua_tointeger(ls, 4);
	int lParam = lua_tointeger(ls, 5);
	LRESULT result;
	wkeFireWindowsMessage(cip->window, (HWND)hWnd, message, wParam, lParam, &result);
	lua_pushinteger(ls, result);
	return 1;
}

int lua_wke_setfous(lua_State* ls) {
	GETCIP(1);
	wkeSetFocus(cip->window);
	return 0;
}

int lua_wke_killfocus(lua_State* ls) {
	GETCIP(1);
	wkeKillFocus(cip->window);
	return 0;
}

int lua_wke_globalexec(lua_State* ls) {
	GETCIP(1);
	lua_pushlightuserdata(ls, wkeGlobalExec(cip->window));
	return 1;
}

int lua_wke_sleep(lua_State* ls) {
	GETCIP(1);
	wkeSleep(cip->window);
	return 0;
}

int lua_wke_wake(lua_State* ls) {
	GETCIP(1);
	wkeWake(cip->window);
	return 0;
}

int lua_wke_isawake(lua_State* ls) {
	GETCIP(1);
	lua_pushboolean(ls, wkeIsAwake(cip->window));
	return 1;
}

int lua_wke_setzoomfactor(lua_State* ls) {
	GETCIP(2);
	float factor = lua_tonumber(ls, 2);
	wkeSetZoomFactor(cip->window, factor);
	return 0;
}

int lua_wke_getzoomfactor(lua_State* ls) {
	GETCIP(1);
	lua_pushnumber(ls, wkeGetZoomFactor(cip->window));
	return 1;
}

int lua_wke_seteditable(lua_State* ls) {
	GETCIP(2);
	bool editable = lua_toboolean(ls, 2);
	wkeSetEditable(cip->window, editable);
	return 0;
}

int lua_wke_getcursorinfotype(lua_State* ls) {
	GETCIP(1);
	lua_pushnumber(ls, wkeGetCursorInfoType(cip->window));
	return 1;
}

int lua_wke_getwindowhandle(lua_State* ls) {
	GETCIP(1);
	lua_pushinteger(ls, (int)wkeGetWindowHandle(cip->window));
	return 1;
}

int lua_wke_showwindow(lua_State* ls) {
	GETCIP(2);
	bool showFlag = lua_toboolean(ls, 2);
	wkeShowWindow(cip->window, showFlag);
	return 0;
}

int lua_wke_enablewindow(lua_State* ls) {
	GETCIP(2);
	bool enableFlag = lua_toboolean(ls, 2);
	wkeEnableWindow(cip->window, enableFlag);
	return 0;
}

int lua_wke_movewindow(lua_State* ls) {
	GETCIP(5);
	int x = lua_tointeger(ls, 2);
	int y = lua_tointeger(ls, 3);
	int w = lua_tointeger(ls, 4);
	int h = lua_tointeger(ls, 5);
	wkeMoveWindow(cip->window, x, y, w, h);
	return 0;
}

int lua_wke_setdeviceparameter(lua_State* ls) {
	GETCIP(5);
	const char* device = lua_tostring(ls, 2);
	const char* paramStr = lua_tostring(ls, 3);
	int paramInt = lua_tointeger(ls, 4);
	float paramFloat = lua_tonumber(ls, 5);
	wkeSetDeviceParameter(cip->window, device, paramStr, paramInt, paramFloat);
	return 0;
}

int lua_wke_setmouseenabled(lua_State* ls) {
	GETCIP(2);
	bool b = lua_toboolean(ls, 2);
	wkeSetMouseEnabled(cip->window, b);
	return 0;
}

int lua_wke_setnavigationtonewwindowenable(lua_State* ls) {
	GETCIP(2);
	bool b = lua_toboolean(ls, 2);
	wkeSetNavigationToNewWindowEnable(cip->window, b);
	return 0;
}

int lua_wke_setcspcheckenable(lua_State* ls) {
	GETCIP(2);
	bool b = lua_toboolean(ls, 2);
	wkeSetCspCheckEnable(cip->window, b);
	return 0;
}

int lua_wke_setnpapipluginsenabled(lua_State* ls) {
	GETCIP(2);
	bool b = lua_toboolean(ls, 2);
	wkeSetNpapiPluginsEnabled(cip->window, b);
	return 0;
}

int lua_wke_setdebugconfig(lua_State* ls) {
	GETCIP(3);
	wkeSetDebugConfig(cip->window, lua_tostring(ls, 2), lua_tostring(ls, 3));
	return 0;
}

int lua_wke_setuseragent(lua_State* ls) {
	GETCIP(2);
	wkeSetUserAgent(cip->window, lua_tostring(ls, 2));
	return 0;
}

int lua_wke_getuseragent(lua_State* ls) {
	GETCIP(1);
	lua_pushstring(ls, wkeGetUserAgent(cip->window));
	return 1;
}

int lua_wke_loadurl(lua_State* ls) {
	GETCIP(2);
	wkeLoadURL(cip->window, lua_tostring(ls, 2));
	return 0;
}

int lua_wke_loadhtml(lua_State* ls) {
	GETCIP(2);
	wkeLoadHTML(cip->window, lua_tostring(ls, 2));
	return 0;
}

int lua_wke_loadhtmlwithbaseurl(lua_State* ls) {
	GETCIP(3);
	const utf8* html = lua_tostring(ls, 2);
	const utf8* baseUrl = lua_tostring(ls, 3);
	wkeLoadHtmlWithBaseUrl(cip->window, html, baseUrl);
	return 0;
}

int lua_wke_loadfile(lua_State* ls) {
	GETCIP(2);
	const utf8* filename = lua_tostring(ls, 2);
	wkeLoadFile(cip->window, filename);
	return 0;
}

int lua_wke_geturl(lua_State* ls) {
	GETCIP(1);
	lua_pushstring(ls, wkeGetURL(cip->window));
	return 1;
}

int lua_wke_setlanguage(lua_State* ls) {
	GETCIP(2);
	const char* lang = lua_tostring(ls, 2);
	wkeSetLanguage(cip->window, lang);
	return 0;
}

int lua_wke_luaenabled(lua_State* ls) {
	GETCIP(2);
	bool b = lua_toboolean(ls, 2);
	cip->luaen = b;
	return 0;
}

int lua_wke_jscallback(lua_State* ls) {
	GETCIP(3);
	const char* szFn = lua_tostring(ls, 2);
	struct luacb_t *cb = (luacb_t*)malloc(sizeof(struct luacb_t));
	cb->cip = cip;
	cb->pFn = luaL_ref(ls, LUA_REGISTRYINDEX);
	wkeJsBindFunction(szFn, load_luacbgsetter, cb, 1);
	return 0;
}

int lua_wke_jsgetter(lua_State* ls) {
	GETCIP(3);
	const char* szFn = lua_tostring(ls, 2);
	struct luacb_t *cb = (luacb_t*)malloc(sizeof(struct luacb_t));
	cb->cip = cip;
	cb->pFn = luaL_ref(ls, LUA_REGISTRYINDEX);
	wkeJsBindGetter(szFn, load_luacbgsetter, cb);
	return 0;
}

int lua_wke_jssetter(lua_State* ls) {
	GETCIP(3);
	const char* szFn = lua_tostring(ls, 2);
	struct luacb_t *cb = (luacb_t*)malloc(sizeof(struct luacb_t));
	cb->cip = cip;
	cb->pFn = luaL_ref(ls, LUA_REGISTRYINDEX);
	wkeJsBindSetter(szFn, load_luacbgsetter, cb);
	return 0;
}

int lua_wke_setproxy(lua_State* ls) {
	GETCIP(6);
	wkeProxy prox;
	prox.type = (wkeProxyType)lua_tointeger(ls, 2);
	sprintf(prox.hostname, "%s", lua_tostring(ls, 3));
	prox.port = lua_tointeger(ls, 4);
	sprintf(prox.username, "%s", lua_tostring(ls, 5));
	sprintf(prox.password, "%s", lua_tostring(ls, 6));
	wkeSetProxy(&prox);
	return 0;
}

int lua_wke_stop(lua_State* ls) {
	GETCIP(1);
	wkeStopLoading(cip->window);
	return 0;
}

int lua_wke_reload(lua_State* ls) {
	GETCIP(1);
	wkeReload(cip->window);
	return 0;
}

int lua_wke_showdevtools(lua_State* ls) {
	GETCIP(2);
	const char* path = lua_tostring(ls, 2);
	wchar_t wpath[MAX_PATH];
	MultiByteToWideChar(CP_ACP, NULL, path, -1, wpath, MAX_PATH);
	wkeShowDevtools(cip->window, wpath, NULL, NULL);
	delete wpath;
	return 0;
}

int lua_wke_addpluginpath(lua_State* ls) {
	GETCIP(2);
	const char* path = lua_tostring(ls, 2);
	wchar_t wpath[MAX_PATH];
	MultiByteToWideChar(CP_ACP, NULL, path, -1, wpath, MAX_PATH);
	wkeAddPluginDirectory(cip->window, wpath);
	delete wpath;
	return 0;
}

int lua_wke_cpath(lua_State* ls) {
	GETCIP(2);
	const char* path = lua_tostring(ls, 2);
	wchar_t wpath[MAX_PATH];
	MultiByteToWideChar(CP_ACP, NULL, path, -1, wpath, MAX_PATH);
	wkeSetCookieJarFullPath(cip->window, wpath);
	delete wpath;
	return 0;
}

int lua_wke_lspath(lua_State* ls) {
	GETCIP(2);
	const char* path = lua_tostring(ls, 2);
	wchar_t wpath[MAX_PATH];
	MultiByteToWideChar(CP_ACP, NULL, path, -1, wpath, MAX_PATH);
	wkeSetLocalStorageFullPath(cip->window, wpath);
	delete wpath;
	return 0;
}

int lua_wke_getcookiesenabled(lua_State* ls) {
	GETCIP(1);
	lua_pushboolean(ls, wkeIsCookieEnabled(cip->window));
	return 1;
}

int lua_wke_setcookiesenabled(lua_State* ls) {
	GETCIP(2);
	wkeSetCookieEnabled(cip->window, lua_toboolean(ls, 2));
	return 0;
}

//Register Types
void RegisterTypes(lua_State* ls)
{
	lua_getglobal(ls, "WKE");
	#define PUTTYPE(a,b) lua_pushstring(ls, a);lua_pushinteger(ls, b);lua_settable(ls, -3)
	//ProxyType
	PUTTYPE("PROXY_NONE", wkeProxyType::WKE_PROXY_NONE);
	PUTTYPE("PROXY_HTTP", wkeProxyType::WKE_PROXY_HTTP);
	PUTTYPE("PROXY_SOCKS4", wkeProxyType::WKE_PROXY_SOCKS4);
	PUTTYPE("PROXY_SOCKS4A", wkeProxyType::WKE_PROXY_SOCKS4A);
	PUTTYPE("PROXY_SOCKS5", wkeProxyType::WKE_PROXY_SOCKS5);
	PUTTYPE("PROXY_SOCKS5HOSTNAME", wkeProxyType::WKE_PROXY_SOCKS5HOSTNAME);
	//ConsoleLevels
	PUTTYPE("CONSOLE_DEBUG", wkeConsoleLevel::wkeLevelDebug);
	PUTTYPE("CONSOLE_LOG", wkeConsoleLevel::wkeLevelLog);
	PUTTYPE("CONSOLE_INFO", wkeConsoleLevel::wkeLevelInfo);
	PUTTYPE("CONSOLE_WARNING", wkeConsoleLevel::wkeLevelWarning);
	PUTTYPE("CONSOLE_ERROR", wkeConsoleLevel::wkeLevelError);
	PUTTYPE("CONSOLE_REVOKEDERROR", wkeConsoleLevel::wkeLevelRevokedError);
	PUTTYPE("CONSOLE_LAST", wkeConsoleLevel::wkeLevelLast);
	//MouseFlags
	PUTTYPE("MF_LBUTTON", wkeMouseFlags::WKE_LBUTTON);
	PUTTYPE("MF_RBUTTON", wkeMouseFlags::WKE_RBUTTON);
	PUTTYPE("MF_SHIFT", wkeMouseFlags::WKE_SHIFT);
	PUTTYPE("MF_CONTROL", wkeMouseFlags::WKE_CONTROL);
	PUTTYPE("MF_MBUTTON", wkeMouseFlags::WKE_MBUTTON);
	//KeyFlags
	PUTTYPE("KF_EXTENDED", wkeKeyFlags::WKE_EXTENDED);
	PUTTYPE("KF_REPEAT", wkeKeyFlags::WKE_REPEAT);
	//MouseMsg
	PUTTYPE("MSG_MOUSEMOVE", wkeMouseMsg::WKE_MSG_MOUSEMOVE);
	PUTTYPE("MSG_LBUTTONDOWN", wkeMouseMsg::WKE_MSG_LBUTTONDOWN);
	PUTTYPE("MSG_LBUTTONUP", wkeMouseMsg::WKE_MSG_LBUTTONUP);
	PUTTYPE("MSG_LBUTTONDBLCLK", wkeMouseMsg::WKE_MSG_LBUTTONDBLCLK);
	PUTTYPE("MSG_RBUTTONDOWN", wkeMouseMsg::WKE_MSG_RBUTTONDOWN);
	PUTTYPE("MSG_RBUTTONUP", wkeMouseMsg::WKE_MSG_RBUTTONUP);
	PUTTYPE("MSG_RBUTTONDBLCLK", wkeMouseMsg::WKE_MSG_RBUTTONDBLCLK);
	PUTTYPE("MSG_MBUTTONDOWN", wkeMouseMsg::WKE_MSG_MBUTTONDOWN);
	PUTTYPE("MSG_MBUTTONUP", wkeMouseMsg::WKE_MSG_MBUTTONUP);
	PUTTYPE("MSG_MBUTTONDBLCLK", wkeMouseMsg::WKE_MSG_MBUTTONDBLCLK);
	PUTTYPE("MSG_MOUSEWHEEL", wkeMouseMsg::WKE_MSG_MOUSEWHEEL);
	//SettingMask
	PUTTYPE("SETTING_PROXY", wkeSettingMask::WKE_SETTING_PROXY);
	PUTTYPE("SETTING_PAINTCALLBACK_IN_OTHER_THREAD", wkeSettingMask::WKE_SETTING_PAINTCALLBACK_IN_OTHER_THREAD);
	//CookieCommand
	PUTTYPE("COOKIE_COMMAND_CLEAR_ALL_COOKIES", wkeCookieCommand::wkeCookieCommandClearAllCookies);
	PUTTYPE("COOKIE_COMMAND_CLEAR_SESSION_COOKIES", wkeCookieCommand::wkeCookieCommandClearSessionCookies);
	PUTTYPE("COOKIE_COMMAND_FLUSH_COOKIES_TO_FILE", wkeCookieCommand::wkeCookieCommandFlushCookiesToFile);
	PUTTYPE("COOKIE_COMMAND_RELOAD_COOKIES_FROM_FILE", wkeCookieCommand::wkeCookieCommandReloadCookiesFromFile);
	//NavigationType
	PUTTYPE("NAVIGATION_TYPE_LINKCLICK", wkeNavigationType::WKE_NAVIGATION_TYPE_LINKCLICK);
	PUTTYPE("NAVIGATION_TYPE_FORMSUBMITTE", wkeNavigationType::WKE_NAVIGATION_TYPE_FORMSUBMITTE);
	PUTTYPE("NAVIGATION_TYPE_BACKFORWARD", wkeNavigationType::WKE_NAVIGATION_TYPE_BACKFORWARD);
	PUTTYPE("NAVIGATION_TYPE_RELOAD", wkeNavigationType::WKE_NAVIGATION_TYPE_RELOAD);
	PUTTYPE("NAVIGATION_TYPE_FORMRESUBMITT", wkeNavigationType::WKE_NAVIGATION_TYPE_FORMRESUBMITT);
	PUTTYPE("NAVIGATION_TYPE_OTHER", wkeNavigationType::WKE_NAVIGATION_TYPE_OTHER);
	//CursorInfoType
	PUTTYPE("CURSOR_INFO_POINTER", WkeCursorInfoType::WkeCursorInfoPointer);
	PUTTYPE("CURSOR_INFO_CROSS", WkeCursorInfoType::WkeCursorInfoCross);
	PUTTYPE("CURSOR_INFO_HAND", WkeCursorInfoType::WkeCursorInfoHand);
	PUTTYPE("CURSOR_INFO_IBEAM", WkeCursorInfoType::WkeCursorInfoIBeam);
	PUTTYPE("CURSOR_INFO_WAIT", WkeCursorInfoType::WkeCursorInfoWait);
	PUTTYPE("CURSOR_INFO_HELP", WkeCursorInfoType::WkeCursorInfoHelp);
	PUTTYPE("CURSOR_INFO_EAST_RESIZE", WkeCursorInfoType::WkeCursorInfoEastResize);
	PUTTYPE("CURSOR_INFO_NORTH_RESIZE", WkeCursorInfoType::WkeCursorInfoNorthResize);
	PUTTYPE("CURSOR_INFO_NORTHEAST_RESIZE", WkeCursorInfoType::WkeCursorInfoNorthEastResize);
	PUTTYPE("CURSOR_INFO_NORTHWEST_RESIZE", WkeCursorInfoType::WkeCursorInfoNorthWestResize);
	PUTTYPE("CURSOR_INFO_SOUTH_RESIZE", WkeCursorInfoType::WkeCursorInfoSouthResize);
	PUTTYPE("CURSOR_INFO_SOUTHEAST_RESIZE", WkeCursorInfoType::WkeCursorInfoSouthEastResize);
	PUTTYPE("CURSOR_INFO_SOUTHWEST_RESIZE", WkeCursorInfoType::WkeCursorInfoSouthWestResize);
	PUTTYPE("CURSOR_INFO_WEST_RESIZE", WkeCursorInfoType::WkeCursorInfoWestResize);
	PUTTYPE("CURSOR_INFO_NORTHSOUTH_RESIZE", WkeCursorInfoType::WkeCursorInfoNorthSouthResize);
	PUTTYPE("CURSOR_INFO_EASTWEST_RESIZE", WkeCursorInfoType::WkeCursorInfoEastWestResize);
	PUTTYPE("CURSOR_INFO_NORTHEASTSOUTHWEST_RESIZE", WkeCursorInfoType::WkeCursorInfoNorthEastSouthWestResize);
	PUTTYPE("CURSOR_INFO_NORTHWESTSOUTHEAST_RESIZE", WkeCursorInfoType::WkeCursorInfoNorthWestSouthEastResize);
	PUTTYPE("CURSOR_INFO_COLUMN_RESIZE", WkeCursorInfoType::WkeCursorInfoColumnResize);
	PUTTYPE("CURSOR_INFO_ROW_RESIZE", WkeCursorInfoType::WkeCursorInfoRowResize);
	PUTTYPE("CURSOR_INFO_MIDDLE_PANNING", WkeCursorInfoType::WkeCursorInfoMiddlePanning);
	PUTTYPE("CURSOR_INFO_EAST_PANNING", WkeCursorInfoType::WkeCursorInfoEastPanning);
	PUTTYPE("CURSOR_INFO_NORTH_PANNING", WkeCursorInfoType::WkeCursorInfoNorthPanning);
	PUTTYPE("CURSOR_INFO_NORTHEAST_PANNING", WkeCursorInfoType::WkeCursorInfoNorthEastPanning);
	PUTTYPE("CURSOR_INFO_NORTHWEST_PANNING", WkeCursorInfoType::WkeCursorInfoNorthWestPanning);
	PUTTYPE("CURSOR_INFO_SOUTH_PANNING", WkeCursorInfoType::WkeCursorInfoSouthPanning);
	PUTTYPE("CURSOR_INFO_SOUTHEAST_PANNING", WkeCursorInfoType::WkeCursorInfoSouthEastPanning);
	PUTTYPE("CURSOR_INFO_SOUTHWEST_PANNING", WkeCursorInfoType::WkeCursorInfoSouthWestPanning);
	PUTTYPE("CURSOR_INFO_WEST_PANNING", WkeCursorInfoType::WkeCursorInfoWestPanning);
	PUTTYPE("CURSOR_INFO_MOVE", WkeCursorInfoType::WkeCursorInfoMove);
	PUTTYPE("CURSOR_INFO_VERTICAL_TEXT", WkeCursorInfoType::WkeCursorInfoVerticalText);
	PUTTYPE("CURSOR_INFO_CELL", WkeCursorInfoType::WkeCursorInfoCell);
	PUTTYPE("CURSOR_INFO_CONTEXT_MENU", WkeCursorInfoType::WkeCursorInfoContextMenu);
	PUTTYPE("CURSOR_INFO_ALIAS", WkeCursorInfoType::WkeCursorInfoAlias);
	PUTTYPE("CURSOR_INFO_PROGRESS", WkeCursorInfoType::WkeCursorInfoProgress);
	PUTTYPE("CURSOR_INFO_NO_DROP", WkeCursorInfoType::WkeCursorInfoNoDrop);
	PUTTYPE("CURSOR_INFO_COPY", WkeCursorInfoType::WkeCursorInfoCopy);
	PUTTYPE("CURSOR_INFO_NONE", WkeCursorInfoType::WkeCursorInfoNone);
	PUTTYPE("CURSOR_INFO_NOT_ALLOWED", WkeCursorInfoType::WkeCursorInfoNotAllowed);
	PUTTYPE("CURSOR_INFO_ZOOM_IN", WkeCursorInfoType::WkeCursorInfoZoomIn);
	PUTTYPE("CURSOR_INFO_ZOOM_OUT", WkeCursorInfoType::WkeCursorInfoZoomOut);
	PUTTYPE("CURSOR_INFO_GRAB", WkeCursorInfoType::WkeCursorInfoGrab);
	PUTTYPE("CURSOR_INFO_GRABBING", WkeCursorInfoType::WkeCursorInfoGrabbing);
	PUTTYPE("CURSOR_INFO_CUSTOM", WkeCursorInfoType::WkeCursorInfoCustom);
	//ResourceType
	PUTTYPE("RESOURCE_TYPE_MAIN_FRAME", wkeResourceType::WKE_RESOURCE_TYPE_MAIN_FRAME);
	PUTTYPE("RESOURCE_TYPE_SUB_FRAME", wkeResourceType::WKE_RESOURCE_TYPE_SUB_FRAME);
	PUTTYPE("RESOURCE_TYPE_STYLESHEET", wkeResourceType::WKE_RESOURCE_TYPE_STYLESHEET);
	PUTTYPE("RESOURCE_TYPE_SCRIPT", wkeResourceType::WKE_RESOURCE_TYPE_SCRIPT);
	PUTTYPE("RESOURCE_TYPE_IMAGE", wkeResourceType::WKE_RESOURCE_TYPE_IMAGE);
	PUTTYPE("RESOURCE_TYPE_FONT_RESOURCE", wkeResourceType::WKE_RESOURCE_TYPE_FONT_RESOURCE);
	PUTTYPE("RESOURCE_TYPE_SUB_RESOURCE", wkeResourceType::WKE_RESOURCE_TYPE_SUB_RESOURCE);
	PUTTYPE("RESOURCE_TYPE_OBJECT", wkeResourceType::WKE_RESOURCE_TYPE_OBJECT);
	PUTTYPE("RESOURCE_TYPE_MEDIA", wkeResourceType::WKE_RESOURCE_TYPE_MEDIA);
	PUTTYPE("RESOURCE_TYPE_WORKER", wkeResourceType::WKE_RESOURCE_TYPE_WORKER);
	PUTTYPE("RESOURCE_TYPE_SHARED_WORKER", wkeResourceType::WKE_RESOURCE_TYPE_SHARED_WORKER);
	PUTTYPE("RESOURCE_TYPE_PREFETCH", wkeResourceType::WKE_RESOURCE_TYPE_PREFETCH);
	PUTTYPE("RESOURCE_TYPE_FAVICON", wkeResourceType::WKE_RESOURCE_TYPE_FAVICON);
	PUTTYPE("RESOURCE_TYPE_XHR", wkeResourceType::WKE_RESOURCE_TYPE_XHR);
	PUTTYPE("RESOURCE_TYPE_PING", wkeResourceType::WKE_RESOURCE_TYPE_PING);
	PUTTYPE("RESOURCE_TYPE_SERVICE_WORKER", wkeResourceType::WKE_RESOURCE_TYPE_SERVICE_WORKER);
	PUTTYPE("RESOURCE_TYPE_LAST_TYPE", wkeResourceType::WKE_RESOURCE_TYPE_LAST_TYPE);
	//HttBodyElementType
	PUTTYPE("wkeHttBodyElementTypeData", wkeHttBodyElementType::wkeHttBodyElementTypeData);
	PUTTYPE("wkeHttBodyElementTypeFile", wkeHttBodyElementType::wkeHttBodyElementTypeFile);
	//RequestType
	PUTTYPE("kWkeRequestTypeInvalidation", wkeRequestType::kWkeRequestTypeInvalidation);
	PUTTYPE("kWkeRequestTypeGet", wkeRequestType::kWkeRequestTypeGet);
	PUTTYPE("kWkeRequestTypePost", wkeRequestType::kWkeRequestTypePost);
	PUTTYPE("kWkeRequestTypePut", wkeRequestType::kWkeRequestTypePut);
	//LoadingResult
	PUTTYPE("LOADING_SUCCEEDED", wkeLoadingResult::WKE_LOADING_SUCCEEDED);
	PUTTYPE("LOADING_FAILED", wkeLoadingResult::WKE_LOADING_FAILED);
	PUTTYPE("LOADING_CANCELED", wkeLoadingResult::WKE_LOADING_CANCELED);
	//DownloadOpt
	PUTTYPE("kWkeDownloadOptCancel", wkeDownloadOpt::kWkeDownloadOptCancel);
	PUTTYPE("kWkeDownloadOptCacheData", wkeDownloadOpt::kWkeDownloadOptCacheData);
	//JS Type
	PUTTYPE("JSTYPE_NUMBER", jsType::JSTYPE_NUMBER);
	PUTTYPE("JSTYPE_STRING", jsType::JSTYPE_STRING);
	PUTTYPE("JSTYPE_BOOLEAN", jsType::JSTYPE_BOOLEAN);
	PUTTYPE("JSTYPE_OBJECT", jsType::JSTYPE_OBJECT);
	PUTTYPE("JSTYPE_FUNCTION", jsType::JSTYPE_FUNCTION);
	PUTTYPE("JSTYPE_UNDEFINED", jsType::JSTYPE_UNDEFINED);
	PUTTYPE("JSTYPE_ARRAY", jsType::JSTYPE_ARRAY);
	PUTTYPE("JSTYPE_NULL", jsType::JSTYPE_NULL);

	lua_settop(ls, 0);
}

int lua_js_argcount(lua_State*ls) {
	int argc = jsArgCount((jsExecState)lua_touserdata(ls, 1));
	lua_pushinteger(ls, argc);
	return 1;
}

int lua_js_argtype(lua_State*ls) {
	int at = jsArgType((jsExecState)lua_touserdata(ls, 1), lua_tointeger(ls,2));
	lua_pushinteger(ls, at);
	return 1;
}

int lua_js_arg(lua_State*ls) {
	jsValue v = jsArg((jsExecState)lua_touserdata(ls, 1), lua_tointeger(ls, 2));
	lua_pushlightuserdata(ls, (void*)v);
	return 1;
}

int lua_js_typeof(lua_State*ls) {
	int at = jsTypeOf((jsValue)lua_touserdata(ls, 1));
	lua_pushinteger(ls, at);
	return 1;
}

int lua_js_is(lua_State*ls) {
	bool ret = false;
	int typ = lua_tointeger(ls, 1);
	jsValue v = (jsValue)lua_touserdata(ls, 2);
	ret = ((int)jsTypeOf(v) == typ);
	lua_pushboolean(ls, ret);
	return 1;
}

int lua_js_to(lua_State*ls) {
	jsExecState ctx = (jsExecState)lua_touserdata(ls, 1);
	int typ = lua_tointeger(ls, 2);
	jsValue v = (jsValue)lua_touserdata(ls, 3);
	jstolua(ls, ctx, v, typ);
	return 1;
}

int lua_js_new(lua_State*ls) {
	jsValue ret;
	jsExecState ctx = (jsExecState)lua_touserdata(ls, 1);
	int typ = lua_tointeger(ls, 2);
	switch (typ) {
	case jsType::JSTYPE_NULL:
		ret = jsNull();
		break;
	case jsType::JSTYPE_BOOLEAN:
		ret = jsBoolean(lua_toboolean(ls,3));
		break;
	case jsType::JSTYPE_NUMBER:
		ret = jsDouble(lua_tonumber(ls, 3));
		break;
	case jsType::JSTYPE_STRING:
		ret = jsString(ctx, lua_tostring(ls, 3));
		break;
	default:
		ret = jsNull();
		break;
	}
	lua_pushlightuserdata(ls, (void*)ret);
	return 1;
}

int lua_js_gab(lua_State*ls) {
	jsExecState ctx = (jsExecState)lua_touserdata(ls, 1);
	jsValue v = (jsValue)lua_touserdata(ls, 2);
	wkeMemBuf* mb = jsGetArrayBuffer(ctx, v);
	lua_pushinteger(ls, mb->length);
	lua_pushinteger(ls, mb->size);
	lua_pushlstring(ls, (char*)mb->data, mb->length * mb->size);
	return 3;
}

int lua_js_invoke(lua_State*ls) {
	jsExecState ctx = (jsExecState)lua_touserdata(ls, 1);
	jsValue fn = (jsValue)lua_touserdata(ls, 2);
	jsValue thisobj = (jsValue)lua_touserdata(ls, 3);
	const int argc = (lua_gettop(ls) - 3);
	jsValue *jsv = new jsValue[argc];
	for (int i = 4; i <= argc; i++)
		jsv[i - 4] = (jsValue)lua_touserdata(ls, i);
	jsValue ret = jsCall(ctx, fn, thisobj, jsv, argc);
	lua_pushlightuserdata(ls, (void*)ret);
	return 1;
}

int lua_js_call(lua_State*ls) {
	jsExecState ctx = (jsExecState)lua_touserdata(ls, 1);
	jsValue fn = (jsValue)lua_touserdata(ls, 2);
	const int argc = (lua_gettop(ls) - 2);
	jsValue *jsv = new jsValue[argc];
	for (int i = 3; i <= argc; i++)
		jsv[i - 3] = (jsValue)lua_touserdata(ls, i);
	jsValue ret = jsCallGlobal(ctx, fn, jsv, argc);
	lua_pushlightuserdata(ls, (void*)ret);
	return 1;
}

int lua_js_get(lua_State*ls) {
	jsExecState ctx = (jsExecState)lua_touserdata(ls, 1);
	jsValue obj = (jsValue)lua_touserdata(ls, 2);
	jsValue ret = jsGet(ctx, obj, lua_tostring(ls, 3));
	lua_pushlightuserdata(ls, (void*)ret);
	return 1;
}

int lua_js_set(lua_State*ls) {
	jsExecState ctx = (jsExecState)lua_touserdata(ls, 1);
	jsSet(ctx, (jsValue)lua_touserdata(ls, 2), lua_tostring(ls, 3), (jsValue)lua_touserdata(ls, 4));
	return 0;
}

int lua_js_getat(lua_State*ls) {
	jsExecState ctx = (jsExecState)lua_touserdata(ls, 1);
	jsValue obj = (jsValue)lua_touserdata(ls, 2);
	jsValue ret = jsGetAt(ctx, obj, lua_tointeger(ls, 3));
	lua_pushlightuserdata(ls, (void*)ret);
	return 1;
}

int lua_js_setat(lua_State*ls) {
	jsExecState ctx = (jsExecState)lua_touserdata(ls, 1);
	jsValue obj = (jsValue)lua_touserdata(ls, 2);
	jsSetAt(ctx, obj, lua_tointeger(ls, 3), (jsValue)lua_touserdata(ls, 4));
	return 0;
}

int lua_js_getkeys(lua_State*ls) {
	jsExecState ctx = (jsExecState)lua_touserdata(ls, 1);
	jsValue obj = (jsValue)lua_touserdata(ls, 2);
	jsKeys * kst = jsGetKeys(ctx, obj);
	lua_pushinteger(ls, kst->length);
	for (int i = 0; i < kst->length; i++)
		lua_pushstring(ls, kst->keys[i]);
	return kst->length + 1;
}

int lua_js_getlength(lua_State*ls) {
	jsExecState ctx = (jsExecState)lua_touserdata(ls, 1);
	jsValue obj = (jsValue)lua_touserdata(ls, 2);
	int l = jsGetLength(ctx, obj);
	lua_pushinteger(ls, l);
	return 1;
}

int lua_js_throw(lua_State*ls) {
	jsExecState ctx = (jsExecState)lua_touserdata(ls, 1);
	jsThrowException(ctx, lua_tostring(ls, 2));
	return 1;
}

int lua_js_setlength(lua_State*ls) {
	jsExecState ctx = (jsExecState)lua_touserdata(ls, 1);
	jsValue obj = (jsValue)lua_touserdata(ls, 2);
	jsSetLength(ctx, obj, lua_tointeger(ls, 3));
	return 0;
}

int CIRPluginObject::RegisterLUAFunctions(lua_State* L)
{
	m_pLuaState = L;
	const struct luaL_Reg pFun[] = {
		{ "GetTitle", lua_wke_gettitle },
		{ "CanGoBack", lua_wke_cangoback },
		{ "GoBack", lua_wke_goback },
		{ "Resize", lua_wke_resize },
		{ "GetWidth", lua_wke_getwidth },
		{ "GetHeight", lua_wke_getheight },
		{ "GetContentWidth", lua_wke_getcontentwidth },
		{ "GetContentHeight", lua_wke_getcontentheight },
		{ "CanGoForward", lua_wke_cangoforward },
		{ "GoForward", lua_wke_goforward },
		{ "EditorSelectAll", lua_wke_selectall },
		{ "EditorUnSelect", lua_wke_unselect },
		{ "EditorCopy", lua_wke_copy },
		{ "EditorCut", lua_wke_cut },
		{ "EditorDelete", lua_wke_delete },
		{ "EditorUndo", lua_wke_undo },
		{ "EditorRedo", lua_wke_redo },
		{ "GetCookie", lua_wke_getcookie },
		{ "SetCookie", lua_wke_setcookie },
		{ "SetMediaVolume", lua_wke_setmediavolume },
		{ "GetMediaVolume", lua_wke_getmediavolume },
		{ "FireMouseEvent", lua_wke_firemouseevent },
		{ "FireMouseWheelEvent", lua_wke_firemousewheelevent },
		{ "FireKeyUpEvent", lua_wke_firekeyupevent },
		{ "FireKeyDownEvent", lua_wke_firedownevent },
		{ "FireWindowsMessage", lua_wke_firewindowsmessage },
		{ "SetFocus", lua_wke_setfous },
		{ "KillFocus", lua_wke_killfocus },
		{ "RunJS", lua_wke_runjs },
		{ "GlobalExec", lua_wke_globalexec },
		{ "Sleep", lua_wke_sleep },
		{ "Wake", lua_wke_wake },
		{ "IsAwake", lua_wke_isawake },
		{ "SetZoomFactor", lua_wke_setzoomfactor },
		{ "GetZoomFactor", lua_wke_getzoomfactor },
		{ "SetEditable", lua_wke_seteditable },
		{ "GetCursorInfoType", lua_wke_getcursorinfotype },
		{ "GetWindowHandle", lua_wke_getwindowhandle },
		{ "ShowWindow", lua_wke_showwindow },
		{ "EnableWindow", lua_wke_enablewindow },
		{ "MoveWindow", lua_wke_movewindow },
		{ "SetDeviceParameter", lua_wke_setdeviceparameter },
		{ "SetMouseEnabled", lua_wke_setmouseenabled },
		{ "SetNavigationToNewWindowEnable", lua_wke_setnavigationtonewwindowenable },
		{ "SetCspCheckEnable", lua_wke_setcspcheckenable },
		{ "SetNpapiPluginsEnabled", lua_wke_setnpapipluginsenabled },
		{ "SetDebugConfig", lua_wke_setdebugconfig },
		{ "SetUserAgent", lua_wke_setuseragent },
		{ "GetUserAgent", lua_wke_getuseragent },
		{ "LoadURL", lua_wke_loadurl },
		{ "LoadHTML", lua_wke_loadhtml },
		{ "LoadHtmlWithBaseUrl", lua_wke_loadhtmlwithbaseurl },
		{ "LoadFile", lua_wke_loadfile },
		{ "GetURL", lua_wke_geturl },
		{ "LuaEnabled", lua_wke_luaenabled },
		{ "SetProxy", lua_wke_setproxy },
		{ "BindFunction", lua_wke_jscallback },
		{ "Stop", lua_wke_stop },
		{ "Reload", lua_wke_stop },
		{ "ShowDevTools", lua_wke_showdevtools },
		{ "GetCookiesEnabled", lua_wke_getcookiesenabled },
		{ "SetCookiesEnabled", lua_wke_setcookiesenabled },
		{ "SetLocalStoragePath", lua_wke_lspath },
		{ "SetCookiePath", lua_wke_cpath },
		{ "EditorPaste", lua_wke_paste },
		{ "BindGetter", lua_wke_jsgetter },
		{ "BindSetter", lua_wke_jssetter },
		{ NULL, NULL }
	};
	luaL_register(L, "WKE", pFun);
	const struct luaL_Reg pFjs[] = {
		{ "ArgCount", lua_js_argcount },
		{ "ArgType", lua_js_argtype },
		{ "Arg", lua_js_arg },
		{ "TypeOf", lua_js_typeof },
		{ "Is", lua_js_is },
		{ "To", lua_js_to },
		{ "New", lua_js_new },
		{ "GetArrayBuffer", lua_js_gab },
		{ "Invoke", lua_js_invoke },
		{ "Call", lua_js_call },
		{ "Get", lua_js_get },
		{ "Set", lua_js_set },
		{ "GetAt", lua_js_getat },
		{ "SetAt", lua_js_setat },
		{ "GetKeys", lua_js_getkeys },
		{ "GetLength", lua_js_getlength },
		{ "SetLength", lua_js_setlength },
		{ "Throw", lua_js_throw },
		{ NULL, NULL }
	};
	luaL_register(L, "JS", pFjs);
	RegisterTypes(L);
	return 0;
}

void CIRPluginObject::FireEvent(LPCTSTR strEventName, LPCTSTR strArguments)
{
	if (!m_pLuaState) return;
	lua_getglobal(m_pLuaState, "Page");
	lua_pushstring(m_pLuaState, "FireEvent");
	lua_gettable(m_pLuaState, -2);
	lua_remove(m_pLuaState, -2);
	if (lua_isfunction(m_pLuaState, -1)) {
		lua_pushnumber(m_pLuaState, (double)((DWORD)this));
		lua_pushstring(m_pLuaState, strEventName);
		lua_pushstring(m_pLuaState, strArguments);
		if (lua_pcall(m_pLuaState, 3, 0, 0) != 0)
			lua_remove(m_pLuaState, -1);
	} else
		lua_remove(m_pLuaState, -1);
}

void CIRPluginObject::DoSetFocus()
{
	if (m_hWnd != NULL) 
		SetFocus(m_hWnd);
	if (window != NULL)
		wkeSetFocus(window);
}

int CIRPluginObject::GetObjectID(char* szBuffer, int* pnBufferSize)
{
	int nLength = lstrlen(m_szObjectID);
	if (*pnBufferSize < nLength) {
		*pnBufferSize = nLength;
		return -1;
	} else {
		memset(szBuffer, 0, *pnBufferSize);
		lstrcpy(szBuffer, m_szObjectID);
		return nLength;
	}
}

CIRPluginObject* IRLUA_PLUGIN_GetObjectPtr(lua_State *luaState, LPCTSTR strObjectName)
{
	CIRPluginObject* pRet = NULL;
	DWORD dwObjectAddress;
	if (luaState) {
		lua_getglobal(luaState, "Page");
		lua_pushstring(luaState, "GetPluginObjectPtr");
		lua_gettable(luaState, -2);
		lua_remove(luaState, -2);
		if (lua_isfunction(luaState, -1)) {
			lua_pushstring(luaState, strObjectName);
			if (lua_pcall(luaState, 1, 1, 0) != 0) {
				lua_remove(luaState, -1);
			} else {
				if (lua_isnumber(luaState, -1)) {
					dwObjectAddress = (DWORD)lua_tonumber(luaState, -1);
					if (dwObjectAddress != 0) 
						pRet = (CIRPluginObject*)dwObjectAddress;
				}
				lua_remove(luaState, -1);
			}
		} else
			lua_remove(luaState, -1);
	}
	return pRet;
}

void IRLUA_PLUGIN_RedrawObject(lua_State *luaState, LPCTSTR strObjectName)
{
	if (luaState) {
		lua_getglobal(luaState, "Page");
		lua_pushstring(luaState, "RedrawObject");
		lua_gettable(luaState, -2);
		lua_remove(luaState, -2);
		if (lua_isfunction(luaState, -1)) {
			lua_pushstring(luaState, strObjectName);
			if (lua_pcall(luaState, 1, 0, 0) != 0)
				lua_remove(luaState, -1);
		} else 
			lua_remove(luaState, -1);
	}
}