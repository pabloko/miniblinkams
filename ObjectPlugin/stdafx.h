// stdafx.h: archivo de inclusi�n de los archivos de inclusi�n est�ndar del sistema
// o archivos de inclusi�n espec�ficos de un proyecto utilizados frecuentemente,
// pero rara vez modificados
//

#pragma once

// La inclusi�n de SDKDDKVer.h define la plataforma Windows m�s alta disponible.

// Si desea compilar la aplicaci�n para una plataforma Windows anterior, incluya WinSDKVer.h y
// establezca la macro _WIN32_WINNT en la plataforma que desea admitir antes de incluir SDKDDKVer.h.

#ifdef DEBUG        
#error "DEBUG configuration WRONG"
#elif NDEBUG   
#pragma message ( "Configuration OK" )
#else
#error "Unknown configuration DEFINITELY WRONG"
#endif

#include <SDKDDKVer.h>

#define WIN32_LEAN_AND_MEAN             // Excluir material rara vez utilizado de encabezados de Windows
// Archivos de encabezado de Windows:
#include <windows.h>
#include "wke.h"
#include <string>
#include <atlstr.h>
#include "lua.hpp"
#include "IRPluginObject.h"
#include <Shellapi.h>

extern void Log(CHAR * szFormat, ...);


// TODO: mencionar aqu� los encabezados adicionales que el programa necesita
